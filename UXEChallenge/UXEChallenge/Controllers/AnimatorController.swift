//
//  AnimatorController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

protocol TransitionProtocol {
    
    func tranisitionSetup()
    func tranisitionCleanup()
    func imageWindowFrame() -> CGRect
    
}

extension UIViewAnimationCurve {
    
    static func animationOptions(fromAnimationCurve curve: UIViewAnimationCurve) -> UIViewAnimationOptions {
        switch (curve) {
        case .easeInOut: return UIViewAnimationOptions.curveEaseInOut
        case .easeIn: return UIViewAnimationOptions.curveEaseIn
        case .easeOut: return UIViewAnimationOptions.curveEaseOut
        case .linear: return UIViewAnimationOptions.curveLinear
        }
    }
    
}

class AnimatorController: NSObject, UIViewControllerAnimatedTransitioning {
    
    static let sharedInstance = AnimatorController()
    
    private var fromDelegate: TransitionProtocol?
    private var toDelegate: TransitionProtocol?
    
    var chosenZoomEffect = AnimationZoomEffect.yes
    var chosenDuration = 0.4
    var chosenDirection = AnimationDirection.fromBottom
    var chosenFade = AnimationFade.yes
    var chosenCurve = UIViewAnimationCurve.easeOut
    var chosenAnimateBar = AnimationBars.yes
    var chosenDamping = 1.0
    
//    let durationsArray = (0 ... 20).map { i in Double(i)/20.0 }
    let durationsArray = [0, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.7, 0.8, 1.0, 1.5, 2.0, 3.0, 5.0, 10.0]
    let dampingArray = (1 ... 20).map { i in Double(i)/20.0 }
    
    private var image: UIImage?
    var selectedIndex: Int?
    
    var presenting = true
    
    
    func setupTransition(image: UIImage?, fromDelegate: TransitionProtocol, toDelegate: TransitionProtocol, presenting: Bool){
        self.fromDelegate = fromDelegate
        self.toDelegate = toDelegate
        self.image = image
        self.presenting = presenting
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.chosenDuration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        // Set up presenting and presented screens
        guard let fromDelegate = self.fromDelegate else { return }
        guard let toDelegate = self.toDelegate else { return }
        fromDelegate.tranisitionSetup()
        toDelegate.tranisitionSetup()
        
        // Get transition context containerView
        let transitionContainerView = transitionContext.containerView

        // Get View Controllers
        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) else { return }
        guard let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else { return }
        guard let detailVC = (self.presenting ? toVC : fromVC) as? PageViewController else { return }
        guard let singlePhotoViewController = detailVC.viewControllers?[0] as? SinglePhotoViewController  else { return }

        // Get Views
        guard let fromView = fromVC.view else { return }
        guard let toView = toVC.view else { return }
        guard let detailView = detailVC.view else { return }
        
        // Order views
        self.presenting ? transitionContainerView.addSubview(toView) : transitionContainerView.insertSubview(toView, belowSubview: fromView)
        
        // Get final frames
        let toDetailImageViewFrame = toDelegate.imageWindowFrame()
        var toDetailViewFrame: CGRect
        if chosenDirection == AnimationDirection.fromLeft {
            toDetailViewFrame = CGRect(x: fromView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height)
        } else if chosenDirection == AnimationDirection.fromBottom {
            toDetailViewFrame = CGRect(x: 0, y: fromView.frame.height, width: toView.frame.width, height: toView.frame.height)
        } else {
            toDetailViewFrame = toView.frame
        }
        toView.frame = self.presenting ?  toDetailViewFrame : toView.frame
        
        // Get screen static views
        guard let detailViewImage = singlePhotoViewController.imageView else { return }

        // Create transition views
        //   Image transition view
        let imageTransitionView = self.chosenZoomEffect == AnimationZoomEffect.yes  ? UIImageView(image: image) : nil
        if self.chosenZoomEffect == AnimationZoomEffect.yes {
            if let imageTransitionView = imageTransitionView {
                imageTransitionView.contentMode = .scaleAspectFill
                imageTransitionView.frame = fromDelegate.imageWindowFrame()
                imageTransitionView.clipsToBounds = true
                transitionContainerView.insertSubview(imageTransitionView, belowSubview: detailView)
            }
        }
        //   Background transition view
        let backgroundTransitionView = UIView(frame: detailView.frame)
        backgroundTransitionView.backgroundColor = singlePhotoViewController.view.backgroundColor
        transitionContainerView.insertSubview(backgroundTransitionView, belowSubview: detailView)
        
        // Set initial opacities
        if self.chosenZoomEffect == AnimationZoomEffect.yes {
            detailViewImage.alpha = 0
        } else {
            detailViewImage.alpha = 1
        }
        if self.chosenFade == AnimationFade.yes {
            toView.alpha = self.presenting ? 0 : 1
            backgroundTransitionView.alpha = detailView.alpha
        }

        // Animations variables
        let duration = transitionDuration(using: transitionContext)
        let options = UIViewAnimationCurve.animationOptions(fromAnimationCurve: self.chosenCurve)

        // Animation closures
        let animationCode = {
            
            // Animate detail view
            detailView.frame = self.presenting ? fromView.frame : toDetailViewFrame
            backgroundTransitionView.frame = detailView.frame
            
            // Set opacities
            if self.chosenFade == AnimationFade.yes {
                detailView.alpha = self.presenting ? 1 : 0
                backgroundTransitionView.alpha = detailView.alpha
            }
            
            // Zoom image its final image frame
            if self.chosenZoomEffect == AnimationZoomEffect.yes {
                if let imageTransitionView = imageTransitionView {
                    imageTransitionView.frame = toDetailImageViewFrame
                }
            }
            
        }
        
        let animationCompletionCode = {

            // Remove transition views and make the screen static ones visible
            if self.chosenZoomEffect == AnimationZoomEffect.yes {
                if let imageTransitionView = imageTransitionView {
                    imageTransitionView.removeFromSuperview()
                }
                detailViewImage.alpha = 1
            }
            backgroundTransitionView.removeFromSuperview()
            
            toDelegate.tranisitionCleanup()
            fromDelegate.tranisitionCleanup()
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            
            // If modal presentation style = .over(FullScreen or Context), fromVC view disappears from the window
            // https://stackoverflow.com/questions/24338700/from-view-controller-disappears-using-uiviewcontrollercontexttransitioning
            if let navController = toVC as? UINavigationController {
                if let _ = navController.viewControllers[1] as? GalleryViewController {
                    UIApplication.shared.keyWindow?.addSubview(toVC.view)
                }
            }
            
        }

        // Animate with damping (less than 1) kills the animation curves
        if self.chosenDamping == 1 {
            UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
                animationCode()
            }, completion:{ _ in
                animationCompletionCode()
            })
        } else {
            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: CGFloat(self.chosenDamping), initialSpringVelocity: 1.0, options: options, animations: {
                animationCode()
            }, completion:{ _ in
                animationCompletionCode()
            })
        }
        
    }
    
    
}

