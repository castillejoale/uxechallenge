//
//  IdolController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

class IdolController: NSObject {
    
    static let sharedInstance = IdolController()
    static let defaultIdols: [Idol] = { return parseIdols() }()
    
    
    private static func parseIdols() -> [Idol] {
        
        guard let fileURL = Bundle.main.url(forResource: "IdolsData", withExtension: "plist") else {
            return []
        }
        
        do {
            let idolData = try Data(contentsOf: fileURL, options: .mappedIfSafe)
            var idols = try PropertyListDecoder().decode([Idol].self, from: idolData)
            idols.sort() { $0.name < $1.name }
            if let testIdol = IdolController.getTestIdol() {
                idols.append(testIdol)
            }
            return idols
        } catch {
            print(error)
            return []
        }
        
    }
    
    private static func getTestIdol() -> Idol? {
        
        guard let testImage = UIImage(named: "rotationGuideLines") else { return nil }
        return Idol(name: "Zoom In + Rotation Test", image: testImage)
        
    }
    

}

