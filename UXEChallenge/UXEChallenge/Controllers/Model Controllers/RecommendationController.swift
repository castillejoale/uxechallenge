//
//  RecommendationController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/12/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

class RecommendationController: NSObject {
    
//    static let sharedInstance = RecommendationController()
    static let recommendations: [Recommendation] = { return getRecommendations() }()
    
    
    private static func getRecommendations() -> [Recommendation] {
        
        var recommendationArray = [Recommendation]()
        
        let recommendation1 = Recommendation(zoomEffect: AnimationZoomEffect.yes, duration: AnimatorController.sharedInstance.durationsArray[12], direction: AnimationDirection.none, fade: AnimationFade.yes, curve: UIViewAnimationCurve.easeOut, animatedBars: AnimationBars.no, damping: AnimatorController.sharedInstance.dampingArray[12])
        let recommendation2 = Recommendation(zoomEffect: AnimationZoomEffect.yes, duration: AnimatorController.sharedInstance.durationsArray[10], direction: AnimationDirection.fromLeft, fade: AnimationFade.yes, curve: UIViewAnimationCurve.easeInOut, animatedBars: AnimationBars.yes, damping: AnimatorController.sharedInstance.dampingArray[19])
        let recommendation3 = Recommendation(zoomEffect: AnimationZoomEffect.no, duration: AnimatorController.sharedInstance.durationsArray[7], direction: AnimationDirection.fromBottom, fade: AnimationFade.no, curve: UIViewAnimationCurve.easeInOut, animatedBars: AnimationBars.yes, damping: AnimatorController.sharedInstance.dampingArray[19])
        
        recommendationArray.append(recommendation1)
        recommendationArray.append(recommendation2)
        recommendationArray.append(recommendation3)
        
        return recommendationArray
        
    }
    

}

