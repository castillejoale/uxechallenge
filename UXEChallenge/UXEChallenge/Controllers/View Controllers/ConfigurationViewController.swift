//
//  ConfigurationViewController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/12/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

extension ConfigurationViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == zoomEffectPickerContainerView.picker {
            return AnimationZoomEffect.count
        } else if pickerView == durationPickerContainerView.picker {
            return AnimatorController.sharedInstance.durationsArray.count
        } else if pickerView == presentingDirectionPickerContainerView.picker {
            return AnimationDirection.allCases.count
        } else if pickerView == fadePickerContainerView.picker {
            return AnimationFade.count
        } else if pickerView == motionCurveContainerView.picker {
            return UIViewAnimationCurve.count
        } else if pickerView == barsAnimationPickerContainerView.picker {
            return AnimationBars.count
        } else if pickerView == dampingPickerContainerView.picker {
            return AnimatorController.sharedInstance.dampingArray.count
        } else {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel: UILabel? = (view as? UILabel)
        
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Helvetica Neue", size: 14)
            pickerLabel?.textAlignment = .center
        }
        
        var labelText: String?
        
        if pickerView == zoomEffectPickerContainerView.picker {
            labelText = AnimationZoomEffect(intValue: row)?.description
        } else if pickerView == durationPickerContainerView.picker {
            labelText = String(AnimatorController.sharedInstance.durationsArray[row])
        } else if pickerView == presentingDirectionPickerContainerView.picker {
            labelText = AnimationDirection(intValue: row)?.rawValue
        } else if pickerView == fadePickerContainerView.picker {
            labelText = AnimationFade(intValue: row)?.description
        } else if pickerView == motionCurveContainerView.picker {
            labelText = UIViewAnimationCurve(intValue: row)?.description
        } else if pickerView == barsAnimationPickerContainerView.picker {
            labelText = AnimationBars(intValue: row)?.description
        } else if pickerView == dampingPickerContainerView.picker {
            labelText = String(AnimatorController.sharedInstance.dampingArray[row])
        }
        
        pickerLabel?.text = labelText
        
        guard let returnView = pickerLabel else { return UIView() }
        
        return returnView
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        recommendationButton1.isSelected = false
        recommendationButton2.isSelected = false
        recommendationButton3.isSelected = false

        self.enableCurvePicker()
        
        let recommendation = getRecommendationFromPickers()
        
        if recommendation == RecommendationController.recommendations[0] {
            recommendationButton1.isSelected = true
        } else if recommendation == RecommendationController.recommendations[1] {
            recommendationButton2.isSelected = true
        } else if recommendation == RecommendationController.recommendations[2] {
            recommendationButton3.isSelected = true
        }
        
    }
    
}

class ConfigurationViewController: UIViewController {
    
    
    @IBOutlet weak var personalizeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var personalizeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var recommendationTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var presentingDirectionPickerContainerView: PickerContainerView!
    @IBOutlet weak var zoomEffectPickerContainerView: PickerContainerView!
    @IBOutlet weak var fadePickerContainerView: PickerContainerView!
    @IBOutlet weak var barsAnimationPickerContainerView: PickerContainerView!
    @IBOutlet weak var durationPickerContainerView: PickerContainerView!
    @IBOutlet weak var motionCurveContainerView: PickerContainerView!
    @IBOutlet weak var dampingPickerContainerView: PickerContainerView!
    
    @IBOutlet weak var recommendationButton1: UIButton!
    @IBOutlet weak var recommendationButton2: UIButton!
    @IBOutlet weak var recommendationButton3: UIButton!
    
    @IBOutlet weak var testButton: UIButton!
    
    var showedAlert: Bool = false
    
    // MARK: View functions

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setUpPickers()
        
        testButton.layer.cornerRadius = 5
        
        // Set first recommendation
        recommendationWasPressed(recommendationButton1)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            personalizeTopConstraint.constant = 10
            personalizeHeightConstraint.constant = 210
            recommendationTopConstraint.constant = 10
        } else {
            personalizeTopConstraint.constant = 60
            personalizeHeightConstraint.constant = 300
            recommendationTopConstraint.constant = 60
        }
    }
    
    // MARK: Custom view functions
    
    func setUpPickers() {
        
        zoomEffectPickerContainerView.picker.delegate = self
        zoomEffectPickerContainerView.picker.dataSource = self
        
        durationPickerContainerView.picker.delegate = self
        durationPickerContainerView.picker.dataSource = self
        
        presentingDirectionPickerContainerView.picker.delegate = self
        presentingDirectionPickerContainerView.picker.dataSource = self
        
        fadePickerContainerView.picker.delegate = self
        fadePickerContainerView.picker.dataSource = self
        
        motionCurveContainerView.picker.delegate = self
        motionCurveContainerView.picker.dataSource = self
        
        barsAnimationPickerContainerView.picker.delegate = self
        barsAnimationPickerContainerView.picker.dataSource = self
        
        dampingPickerContainerView.picker.delegate = self
        dampingPickerContainerView.picker.dataSource = self
        
        presentingDirectionPickerContainerView.titleLabel.text = "Presenting Direction"
        zoomEffectPickerContainerView.titleLabel.text = "Zoom Effect"
        fadePickerContainerView.titleLabel.text = "Fade"
        barsAnimationPickerContainerView.titleLabel.text = "Bars Animation"
        durationPickerContainerView.titleLabel.text = "Duration"
        motionCurveContainerView.titleLabel.text = "Motion Curve"
        dampingPickerContainerView.titleLabel.text = "Damping"
        
    }
    
    func enableCurvePicker() {
        
        let dampingRow = dampingPickerContainerView.picker.selectedRow(inComponent: 0)
        let damping = AnimatorController.sharedInstance.dampingArray[dampingRow]
        if damping == 1.0 {
            motionCurveContainerView.picker.isUserInteractionEnabled = true
            motionCurveContainerView.picker.alpha = 1
        } else {
            motionCurveContainerView.picker.isUserInteractionEnabled = false
            motionCurveContainerView.picker.alpha = 0.3
        }
        
    }
    
    func setRecommendation(recommendation: Recommendation) {
        
        self.zoomEffectPickerContainerView.picker.selectRow(recommendation.zoomEffect.rawValue, inComponent: 0, animated: true)
        guard let durationRow = AnimatorController.sharedInstance.durationsArray.index(of: recommendation.duration) else { return }
        self.durationPickerContainerView.picker.selectRow(durationRow, inComponent: 0, animated: true)
        self.presentingDirectionPickerContainerView.picker.selectRow(recommendation.direction.integer, inComponent: 0, animated: true)
        
        
        self.fadePickerContainerView.picker.selectRow(recommendation.fade.integer, inComponent: 0, animated: true)
        self.barsAnimationPickerContainerView.picker.selectRow(recommendation.animatedBars.integer, inComponent: 0, animated: true)
        guard let dampingRow = AnimatorController.sharedInstance.dampingArray.index(of: recommendation.damping) else { return }
        self.dampingPickerContainerView.picker.selectRow(dampingRow, inComponent: 0, animated: true)
        
        let damping = AnimatorController.sharedInstance.dampingArray[dampingRow]
        if damping == 1.0 {
            motionCurveContainerView.picker.selectRow(recommendation.curve.integer, inComponent: 0, animated: true)
        }
        
        self.enableCurvePicker()
        
    }
    
    func getRecommendationFromPickers() -> Recommendation? {
        
        let zoomEffectRow = zoomEffectPickerContainerView.picker.selectedRow(inComponent: 0)
        let durationRow = durationPickerContainerView.picker.selectedRow(inComponent: 0)
        let directionRow = presentingDirectionPickerContainerView.picker.selectedRow(inComponent: 0)
        let fadeRow = fadePickerContainerView.picker.selectedRow(inComponent: 0)
        let curveRow = motionCurveContainerView.picker.selectedRow(inComponent: 0)
        let barsAnimatedRow = barsAnimationPickerContainerView.picker.selectedRow(inComponent: 0)
        let dampingRow = dampingPickerContainerView.picker.selectedRow(inComponent: 0)
        
        guard let zoomEffect = AnimationZoomEffect(intValue: zoomEffectRow) else { return nil }
        let duration = AnimatorController.sharedInstance.durationsArray[durationRow]
        guard let direction = AnimationDirection(intValue: directionRow) else { return nil }
        guard let fade = AnimationFade(intValue: fadeRow) else { return nil }
        guard let curve = UIViewAnimationCurve(intValue: curveRow) else { return nil }
        guard let animatedBars = AnimationBars(intValue: barsAnimatedRow) else { return nil }
        let damping = AnimatorController.sharedInstance.dampingArray[dampingRow]
        
        let recommendation = Recommendation(zoomEffect: zoomEffect, duration: duration, direction: direction, fade: fade, curve: curve, animatedBars: animatedBars, damping: damping)
        
        return recommendation
        
    }
    
    // MARK: Navigation functions
    
    func goToNextScreen() {
        
        guard let recommendation = self.getRecommendationFromPickers() else { return }
        
        AnimatorController.sharedInstance.chosenZoomEffect = recommendation.zoomEffect
        AnimatorController.sharedInstance.chosenDuration = recommendation.duration
        AnimatorController.sharedInstance.chosenDirection = recommendation.direction
        AnimatorController.sharedInstance.chosenFade = recommendation.fade
        AnimatorController.sharedInstance.chosenCurve = recommendation.curve
        AnimatorController.sharedInstance.chosenAnimateBar = recommendation.animatedBars
        AnimatorController.sharedInstance.chosenDamping = recommendation.damping
        
        let viewController = GalleryViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    // MARK: IBActions
    
    @IBAction func recommendationWasPressed(_ sender: UIButton) {
        
        var recommendation: Recommendation?
        if sender.tag == 1 {
            recommendation = RecommendationController.recommendations[0]
        } else if sender.tag == 2 {
            recommendation = RecommendationController.recommendations[1]
        } else if sender.tag == 3 {
            recommendation = RecommendationController.recommendations[2]
        }
        
        recommendationButton1.isSelected = false
        recommendationButton2.isSelected = false
        recommendationButton3.isSelected = false
        sender.isSelected = true
        
        if let recommendation = recommendation {
            setRecommendation(recommendation: recommendation)
        }
        
    }
    
    @IBAction func nextWasPressed(_ sender: Any) {
        
        if showedAlert {
            goToNextScreen()
        } else {
            let alert = UIAlertController(title: "Remember", message: "Tap the gallery navigation bar or edge pan left to come back to this screen", preferredStyle: .alert)
            
            self.present(alert, animated: true)
            
            self.showedAlert = true
            
            alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: { action in
                
                self.goToNextScreen()
                
            }))
        }
        
    }

    
}

