//
//  ViewController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/7/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

extension GalleryViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return nil }
        guard let toViewController = presented as? PageViewController else { return nil }
        
        let idol = IdolController.defaultIdols[index]
        
        AnimatorController.sharedInstance.setupTransition(image: idol.image, fromDelegate: self, toDelegate: toViewController, presenting: true)
        
        return AnimatorController.sharedInstance
        
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return nil }
        guard let toViewController = dismissed as? PageViewController else { return nil }
        
        let idol = IdolController.defaultIdols[index]
        
        AnimatorController.sharedInstance.setupTransition(image: idol.image, fromDelegate: toViewController, toDelegate: self, presenting: false)
        
        return AnimatorController.sharedInstance
        
    }
    
}

extension GalleryViewController: TransitionProtocol {
    
    func tranisitionSetup() {
        
        if AnimatorController.sharedInstance.chosenFade == AnimationFade.yes {
            let duration = AnimatorController.sharedInstance.chosenDuration
            let options = UIViewAnimationCurve.animationOptions(fromAnimationCurve: AnimatorController.sharedInstance.chosenCurve)
            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: CGFloat(AnimatorController.sharedInstance.chosenDamping), initialSpringVelocity: 0.8, options: options, animations: {
                self.view.alpha = AnimatorController.sharedInstance.presenting ? 0 : 1
            })
        }
        
        if AnimatorController.sharedInstance.chosenZoomEffect == AnimationZoomEffect.yes{
            hideSelectedCell = true
            collectionView.reloadData()
        }
        
    }
    
    func tranisitionCleanup() {

        if AnimatorController.sharedInstance.chosenZoomEffect == AnimationZoomEffect.yes{
            hideSelectedCell = false
            collectionView.reloadData()
        }
        
    }
    
    // Return window frame of selected image
    @objc func imageWindowFrame() -> CGRect{
        
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return CGRect.zero }
        let indexPath = IndexPath(row: index, section: 0)
        let attributes = collectionView.layoutAttributesForItem(at: indexPath)
        guard let cellRect = attributes?.frame else { return CGRect.zero }
        let result = self.collectionView.convert(cellRect, to: self.view)
        return result
        
    }
    
}

extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return IdolController.defaultIdols.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
        let idol = IdolController.defaultIdols[indexPath.item]
        cell.imageView.image = (indexPath.row == AnimatorController.sharedInstance.selectedIndex && hideSelectedCell) ? nil : idol.image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenRect: CGRect = UIScreen.main.bounds
        let screenWidth: CGFloat = screenRect.size.width - view.safeAreaInsets.left - view.safeAreaInsets.right
        
        let width = CGFloat(screenWidth - 30 - 8) / 2.0 // 30 are the screen margins, 8 is the padding between cells
        let height: CGFloat = width * (206.0/282.0) // Image ratio from photoshop file
        
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        AnimatorController.sharedInstance.selectedIndex = indexPath.item
        selectedCell = collectionView.cellForItem(at: indexPath)
        
        let modalViewController = PageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        modalViewController.modalPresentationStyle = .overFullScreen
        modalViewController.transitioningDelegate = self
        present(modalViewController, animated: true, completion: nil)
        
    }
    
}

class GalleryViewController: UIViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedCell: UICollectionViewCell?
    var hideSelectedCell = false
    
    // MARK: View functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpCollectionView()
        setUpGestures()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillLayoutSubviews() {
        
        collectionView.collectionViewLayout.invalidateLayout()
        
        switch UIDevice.current.orientation{
        case .portrait:
            navBarHeightConstraint.constant = 80
            UIApplication.shared.isStatusBarHidden = false
        case .portraitUpsideDown:
            navBarHeightConstraint.constant = 80
            UIApplication.shared.isStatusBarHidden = false
        case .landscapeLeft:
            navBarHeightConstraint.constant = 60
            UIApplication.shared.isStatusBarHidden = true
        case .landscapeRight:
            navBarHeightConstraint.constant = 60
            UIApplication.shared.isStatusBarHidden = true
        default:
            navBarHeightConstraint.constant = 80
            UIApplication.shared.isStatusBarHidden = false
        }
        
    }
    
    // MARK: Custom view functions
    
    func setUpCollectionView() {
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "collectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
    }
    
    func scrollCollectionViewTo(index: Int) {
        
        let indexPath = IndexPath(row: index, section: 0)
        let attributes = self.collectionView.layoutAttributesForItem(at: indexPath)
        guard let cellRect = attributes?.frame else { return }
        let result = self.collectionView.convert(cellRect, to: self.view)
        let cellsPerRow = 2
        
        if result.origin.y < self.collectionView.frame.origin.y {
            self.collectionView.scrollToItem(at: indexPath, at: .top, animated: false)
            if let flow: UICollectionViewFlowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                let insetTop = indexPath.row < cellsPerRow ? flow.sectionInset.top : self.collectionView.layoutMargins.top
                self.collectionView.contentOffset = CGPoint(x: self.collectionView.contentOffset.x, y: self.collectionView.contentOffset.y - insetTop)
            }
        } else if result.origin.y > self.collectionView.frame.origin.y + self.collectionView.frame.size.height - result.size.height {
            self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: false)
            if let flow: UICollectionViewFlowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                let numberOfCells = self.collectionView.numberOfItems(inSection: 0)
                let cellsInLastRow = numberOfCells % cellsPerRow == 0 ? cellsPerRow : numberOfCells % cellsPerRow
                let insetBottom = indexPath.row >= numberOfCells - cellsInLastRow ? flow.sectionInset.bottom : self.collectionView.layoutMargins.bottom
                self.collectionView.contentOffset = CGPoint(x: self.collectionView.contentOffset.x, y: self.collectionView.contentOffset.y + insetBottom)
            }
        } // Else don't move
        
    }
    
    // MARK: Gesture functions
    
    func setUpGestures() {
        // Tap to come back to configuration screen
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        tap.numberOfTapsRequired = 1
        self.navigationBarView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

