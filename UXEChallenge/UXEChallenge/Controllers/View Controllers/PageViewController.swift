//
//  PageViewController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

extension PageViewController: TransitionProtocol {
    
    func tranisitionSetup() {
        
        if AnimatorController.sharedInstance.chosenAnimateBar == AnimationBars.yes {
            self.showBars(false, duration: 0) // Start closed
        } else {
            self.showBars(true, duration: 0) // Start open
        }
        
        if let singlePhotoViewController = self.viewControllers?[0] as? SinglePhotoViewController {
            singlePhotoViewController.view.backgroundColor = .clear
        }
        
    }
    
    func tranisitionCleanup() {
        
        if AnimatorController.sharedInstance.chosenAnimateBar == AnimationBars.yes {
            self.showBars(true, duration: 0.15) // Show bars after transition ends
        }
        
        if let singlePhotoViewController = self.viewControllers?[0] as? SinglePhotoViewController {
            singlePhotoViewController.view.backgroundColor = .black
        }
        
    }
    
    // Return window frame of selected image
    @objc func imageWindowFrame() -> CGRect{
        
        guard let singlePhotoViewController = self.viewControllers?[0] as? SinglePhotoViewController else {
            return CGRect.zero
        }
        
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return CGRect.zero }
        
        let idol = IdolController.defaultIdols[index]
        let photo = idol.image
        
        singlePhotoViewController.view.layoutSubviews()
        guard let scrollWindowFrame = singlePhotoViewController.view?.convert(singlePhotoViewController.scrollView.frame, to: nil)  else { return CGRect.zero }
        let scrollViewRatio = singlePhotoViewController.scrollView.frame.size.width / singlePhotoViewController.scrollView.frame.size.height
        let imageRatio = photo.size.width / photo.size.height
        let touchesSides = (imageRatio > scrollViewRatio)
        
        var result = CGRect.zero

        if touchesSides {
            let height = scrollWindowFrame.size.width / imageRatio
            let yPoint = scrollWindowFrame.origin.y + (scrollWindowFrame.size.height - height) / 2
            result = CGRect(x: scrollWindowFrame.origin.x, y: yPoint, width: scrollWindowFrame.size.width, height: height)
        } else {
            let width = scrollWindowFrame.size.height * imageRatio
            let xPoint = scrollWindowFrame.origin.x + (scrollWindowFrame.size.width - width) / 2
            result = CGRect(x: xPoint, y: scrollWindowFrame.origin.y, width: width, height: scrollWindowFrame.size.height)
        }
        
        return result

    }
    
}

extension PageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewController = viewController as? SinglePhotoViewController,
            let pageIndex = viewController.pageIndex,
            pageIndex > 0  else {
                return nil
        }
        return viewControllerForPage(at: pageIndex - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewController = viewController as? SinglePhotoViewController,
            let pageIndex = viewController.pageIndex,
            pageIndex + 1 < IdolController.defaultIdols.count else {
                return nil
        }
        return viewControllerForPage(at: pageIndex + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return }
        self.setTitle(index: index)
    }
    
}

extension PageViewController: SinglePhotoNavigationBarDelegate, SinglePhotoFooterDelegate {
    
    func shareWasPressed() {
        
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return }
        let idol = IdolController.defaultIdols[index]
        let title = idol.name
        let image: UIImage = idol.image
        
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [(image), title], applicationActivities: nil)
        self.present(shareVC, animated: true, completion: nil)
        
    }
    
    func closeWasPressed() {
        self.dismiss()
    }
    
}

class PageViewController: UIPageViewController, UIGestureRecognizerDelegate {
    
    var navigationBarContainerView: UIView!
    var footerContainerView: UIView!
    
    var navigationBar: SinglePhotoNavigationBar!
    var footer: SinglePhotoFooter!
    
    var navigationTopConstraint: NSLayoutConstraint!
    var footerBottomConstraint: NSLayoutConstraint!
    
    var showingBars: Bool = false
    
    // MARK: View functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        setUpBars()
        
        setUpGestures()
        
        // Set current View Controller
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return }
        setViewControllers([viewControllerForPage(at: index)], direction: .forward, animated: false, completion: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let index = AnimatorController.sharedInstance.selectedIndex else { return }
        self.setTitle(index: index)
    }
    
    override func viewWillLayoutSubviews() {
        footer.updateInnerConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Necessary because the modal presentation style is .overCurrentContext, this will prevent the gallery view to show when scrolling on the edges of the single photo view
        if let navController = self.presentingViewController as? UINavigationController {
            if let vc = navController.viewControllers[1] as? GalleryViewController {
                vc.view.isHidden = true
            }
        }
    }
    
    // MARK: Custom view functions
    
    func setUpBars() {
        
        let screenRect: CGRect = UIScreen.main.bounds
        let screenWidth: CGFloat = screenRect.size.width
        
        // Navigation Bar
        //   Container View
        navigationBarContainerView = UIView()
        self.view.addSubview(navigationBarContainerView)
        navigationBarContainerView.translatesAutoresizingMaskIntoConstraints = false
        navigationTopConstraint = navigationBarContainerView.topAnchor.constraint(equalTo: view.topAnchor)
        navigationTopConstraint.isActive = true
        navigationBarContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        navigationBarContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        //   View
        navigationBar = SinglePhotoNavigationBar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 60))
        navigationBarContainerView.addSubview(navigationBar)
        navigationBar.delegate = self
        if AnimatorController.sharedInstance.chosenDirection == AnimationDirection.fromLeft {
            navigationBar.setDissmissButton(left: true)
        } else {
            navigationBar.setDissmissButton(left: false)
        }
        //   Auto layout
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.topAnchor.constraint(equalTo: navigationBarContainerView.safeAreaLayoutGuide.topAnchor).isActive = true
        navigationBar.bottomAnchor.constraint(equalTo: navigationBarContainerView.safeAreaLayoutGuide.bottomAnchor).isActive = true
        navigationBar.leadingAnchor.constraint(equalTo: navigationBarContainerView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        navigationBar.trailingAnchor.constraint(equalTo: navigationBarContainerView.safeAreaLayoutGuide.trailingAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: 60).isActive = true
        //   Set color of container view
        navigationBarContainerView.backgroundColor = navigationBar.contentView.backgroundColor
        
        // Footer
        //   Container View
        footerContainerView = UIView()
        self.view.addSubview(footerContainerView)
        footerContainerView.translatesAutoresizingMaskIntoConstraints = false
        footerBottomConstraint = footerContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        footerBottomConstraint.isActive = true
        footerContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        footerContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        //   View
        footer = SinglePhotoFooter(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 45))
        footerContainerView.addSubview(footer)
        footer.delegate = self
        //   Auto layout
        footer.translatesAutoresizingMaskIntoConstraints = false
        footer.topAnchor.constraint(equalTo: footerContainerView.safeAreaLayoutGuide.topAnchor).isActive = true
        footer.bottomAnchor.constraint(equalTo: footerContainerView.safeAreaLayoutGuide.bottomAnchor).isActive = true
        footer.leadingAnchor.constraint(equalTo: footerContainerView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        footer.trailingAnchor.constraint(equalTo: footerContainerView.safeAreaLayoutGuide.trailingAnchor).isActive = true
        footer.heightAnchor.constraint(equalToConstant: 45).isActive = true
        // Set color of container view
        footerContainerView.backgroundColor = footer.contentView.backgroundColor
        
    }
    
    func showBars(_ boolean: Bool, duration: Double) {

        // Important, this way the safe area insets will be included in the height getters
        view.layoutIfNeeded()
        
        self.showingBars = boolean
        if self.showingBars {
            navigationTopConstraint.constant = 0
            footerBottomConstraint.constant = 0
        } else {
            navigationTopConstraint.constant = -navigationBarContainerView.frame.height
            footerBottomConstraint.constant = footerContainerView.frame.height
        }

        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            
            self.view.layoutIfNeeded()
            
            switch UIDevice.current.orientation{
            case .portrait:
                UIApplication.shared.isStatusBarHidden = !self.showingBars
            case .portraitUpsideDown:
                UIApplication.shared.isStatusBarHidden = !self.showingBars
            case .landscapeLeft:
                UIApplication.shared.isStatusBarHidden = true
            case .landscapeRight:
                UIApplication.shared.isStatusBarHidden = true
            default:
                UIApplication.shared.isStatusBarHidden = !self.showingBars
            }
            
        }, completion: nil)
        
    }
    
    func setTitle(index: Int){
        let idol = IdolController.defaultIdols[index]
        self.navigationBar?.setTitle(title: idol.name)
    }
    
    // MARK: Navigation functions
    
    private func viewControllerForPage(at index: Int) -> UIViewController {
        let viewController = SinglePhotoViewController()
        viewController.pageIndex = index
        viewController.idol = IdolController.defaultIdols[index]
        return viewController
    }
    
    func dismiss() {
        
        if let navController = self.presentingViewController as? UINavigationController {
            
            if let vc = navController.viewControllers[1] as? GalleryViewController {
                
                // Necessary because the modal presentation style is .overCurrentContext, this will prevent the gallery view to show when scrolling on the edges of the single photo view
                vc.view.isHidden = false
                
                guard let index = AnimatorController.sharedInstance.selectedIndex else { return }
                // Scroll Gallery Collection View to the Right position
                vc.scrollCollectionViewTo(index:index)
                
                self.dismiss(animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    // MARK: Gesture functions
    
    func setUpGestures() {
        
        // Taps
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap1(_:)))
        tap1.delegate = self
        tap1.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: nil)
        tap2.delegate = self
        tap2.numberOfTapsRequired = 2
        self.view.addGestureRecognizer(tap2)
        tap1.require(toFail: tap2)
        
    }
    
    @objc func handleTap1(_ sender: UITapGestureRecognizer) {
        showingBars = !showingBars
        self.showBars(self.showingBars, duration: 0.15)
    }
    
    
}

