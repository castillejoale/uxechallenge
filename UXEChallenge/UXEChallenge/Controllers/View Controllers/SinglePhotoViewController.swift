//
//  SinglePhotoViewController.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

extension SinglePhotoViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        if let parentVC = self.parent as? PageViewController {
            parentVC.showBars(false, duration: 0.15)
        }
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scrollView.zoomScale == 1 {
            if let parentVC = self.parent as? PageViewController {
                parentVC.showBars(true, duration: 0.15)
            }
        }
        
        updateScrollViewInsets()
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateScrollViewInsets()
    }
    
}

class SinglePhotoViewController: UIViewController {
    
    var pageIndex: Int?
    var idol: Idol?
    
    @IBOutlet weak var commentButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var addButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: View functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpScrollView()
        setUpGestures()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        if let idol = self.idol {
            imageView.image = idol.image
        }

        AnimatorController.sharedInstance.selectedIndex = self.pageIndex
        
        NotificationCenter.default.addObserver(self, selector: #selector(SinglePhotoViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Custom view functions
    
    func setUpScrollView() {
        // Scroll view set up
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 3.0
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.showsVerticalScrollIndicator = false
        // Prevent jump when hiding status bar
        self.scrollView.contentInsetAdjustmentBehavior = .never
    }
    
    func updateScrollViewInsets() {
        
        let zoomScale = scrollView.zoomScale
        
        if zoomScale < 1 {
            
            scrollView.contentInset = UIEdgeInsets.zero
            
        } else if zoomScale <= scrollView.maximumZoomScale {
            
            if let image = imageView.image {
                
                let ratioW = imageView.frame.width / image.size.width
                let ratioH = imageView.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW:ratioH
                
                let newWidth = image.size.width*ratio
                let newHeight = image.size.height*ratio
                
                let left = 0.5 * (newWidth * zoomScale > imageView.frame.width ? (newWidth - imageView.frame.width) : (scrollView.frame.width - scrollView.contentSize.width))
                let top = 0.5 * (newHeight * zoomScale > imageView.frame.height ? (newHeight - imageView.frame.height) : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsetsMake(top, left, top, left)
            }
            
        }
        
    }
    
    func zoomRect(scale: CGFloat, center: CGPoint) -> CGRect {
        
        var zoomRect = CGRect()
        
        zoomRect.size.height = scrollView.frame.size.height / scale
        zoomRect.size.width = scrollView.frame.size.width / scale
        zoomRect.origin.x = center.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0)
        
        return zoomRect
        
    }
    
    // Better than viewWillTransition because it calls updateScrollViewInsets at the right time
    @objc func rotated() {
        updateScrollViewInsets()
        // Since rotating will reset the status bar hidden state
        if let parentVC = self.parent as? PageViewController {
            parentVC.showBars(parentVC.showingBars, duration: 0)
        }
    }
    
    // MARK: Gesture functions
    
    func setUpGestures() {
        
        // Tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.doubleTapped(sender:)))
        tap.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap)
        
        // Swipe gesture if coming from bottom
        if AnimatorController.sharedInstance.chosenDirection == AnimationDirection.fromBottom {
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
            swipeDown.direction = UISwipeGestureRecognizerDirection.down
            self.view.addGestureRecognizer(swipeDown)
        }
        
    }
    
    @objc func doubleTapped(sender: UITapGestureRecognizer) {
        
        let tapPosition = sender.location(in: self.view)
        
        if let parentVC = self.parent as? PageViewController {
            if self.scrollView.zoomScale == self.scrollView.minimumZoomScale {
                parentVC.showBars(false, duration: 0.15)
            } else {
                parentVC.showBars(true, duration: 0.15)
            }
        }
        
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {

            if self.scrollView.zoomScale == self.scrollView.minimumZoomScale {
                let zoomRect: CGRect = self.zoomRect(scale: self.scrollView.maximumZoomScale, center: tapPosition)
                self.scrollView.zoom(to: zoomRect, animated: false)
            } else {
                self.scrollView.setZoomScale(self.scrollView.minimumZoomScale, animated: false) //False because with the UIView.animate I have control over the duration
            }

        }, completion: nil)
        
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                if let parentVC = self.parent as? PageViewController {
                    parentVC.closeWasPressed()
                }
            default:
                break
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func closeWasPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

