//
//  AnimatorEnums.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/12/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

enum AnimationZoomEffect: Int {
    
    case yes = 1
    case no = 0
    
    init?(bool: Bool) {
        self = bool == true ? .yes : .no
    }
    
    init?(intValue: Int) {
        if intValue == 0 {
            self = .yes
        } else if intValue == 1 {
            self = .no
        } else {
            return nil
        }
    }
    
    var description: String {
        switch self {
        case .yes:
            return "Yes"
        case .no:
            return "No"
        }
    }
    
    var boolean: Bool {
        switch self {
        case .yes:
            return true
        case .no:
            return false
        }
    }
    
    static var count = 2
    
}

enum AnimationDirection: String, CaseIterable {
    
    case none = "None"
    case fromLeft = "Left"
    case fromBottom = "Bottom"
    
    init?(intValue: Int) {
        if intValue == 0 {
            self = .none
        } else if intValue == 1 {
            self = .fromLeft
        } else if intValue == 2 {
            self = .fromBottom
        } else {
            return nil
        }
    }
    
    var integer: Int {
        switch self {
        case .none:
            return 0
        case .fromLeft:
            return 1
        case .fromBottom:
            return 2
        }
    }
    
}

enum AnimationFade {
    
    case yes
    case no
    
    init?(boolean: Bool) {
        self = boolean == true ? .yes : .no
    }
    
    init?(intValue: Int) {
        if intValue == 0 {
            self = .yes
        } else if intValue == 1 {
            self = .no
        } else {
            return nil
        }
    }
    
    var description: String {
        switch self {
        case .yes:
            return "Yes"
        case .no:
            return "No"
        }
    }
    
    var boolean: Bool {
        switch self {
        case .yes:
            return true
        case .no:
            return false
        }
    }
    
    var integer: Int {
        switch self {
        case .yes:
            return 0
        case .no:
            return 1
        }
    }
    
    static var count = 2
    
}

extension UIViewAnimationCurve {
    
    var description: String {
        switch self {
        case .easeInOut:
            return "EaseInOut"
        case .easeIn:
            return "EaseIn"
        case .easeOut:
            return "EaseOut"
        case .linear:
            return "Linear"
        }
    }
    
    init?(intValue: Int) {
        if intValue == 0 {
            self = .easeInOut
        } else if intValue == 1 {
            self = .easeIn
        } else if intValue == 2 {
            self = .easeOut
        } else if intValue == 3 {
            self = .linear
        } else {
            return nil
        }
    }
    
    var integer: Int {
        switch self {
        case .easeInOut:
            return 0
        case .easeIn:
            return 1
        case .easeOut:
            return 2
        case .linear:
            return 3
        }
            
    }
    
    static var count = 3
    
}

enum AnimationBars {
    
    case yes
    case no
    
    init?(boolean: Bool) {
        self = boolean == true ? .yes : .no
    }
    
    init?(intValue: Int) {
        if intValue == 0 {
            self = .yes
        } else if intValue == 1 {
            self = .no
        } else {
            return nil
        }
    }
    
    var description: String {
        switch self {
        case .yes:
            return "Yes"
        case .no:
            return "No"
        }
    }
    
    var boolean: Bool {
        switch self {
        case .yes:
            return true
        case .no:
            return false
        }
    }
    
    var integer: Int {
        switch self {
        case .yes:
            return 0
        case .no:
            return 1
        }
    }
    
    static var count = 2
    
}

