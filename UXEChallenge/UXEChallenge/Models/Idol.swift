//
//  Idol.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

enum IdolError: Error {
    case noImage
}

struct Idol: Decodable {
    
    var name: String
    var image: UIImage
    
    enum CodingKeys: String, CodingKey {
        case name
        case image
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        let imageName = try container.decode(String.self, forKey: .image)
        if let fetchedImage = UIImage(named: imageName) {
            image = fetchedImage
        } else {
            throw IdolError.noImage
        }
    }
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
    
    
}

