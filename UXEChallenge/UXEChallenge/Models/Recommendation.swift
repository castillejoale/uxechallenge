//
//  Recommendation.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/12/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

struct Recommendation: Equatable {
    
    let zoomEffect: AnimationZoomEffect
    let duration: Double
    let direction: AnimationDirection
    let fade: AnimationFade
    let curve: UIViewAnimationCurve
    let animatedBars: AnimationBars
    let damping: Double
    
    init(zoomEffect: AnimationZoomEffect,duration: Double, direction: AnimationDirection, fade: AnimationFade, curve: UIViewAnimationCurve, animatedBars: AnimationBars, damping: Double) {
        self.zoomEffect = zoomEffect
        self.duration = duration
        self.direction = direction
        self.fade = fade
        self.curve = curve
        self.animatedBars = animatedBars
        self.damping = damping
    }
    
}
