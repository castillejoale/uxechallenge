//
//  CollectionViewCell.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/8/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.borderWidth = 1.0
        let opacity:CGFloat = 0.15
        let borderColor = UIColor.black
        self.layer.borderColor = borderColor.withAlphaComponent(opacity).cgColor
    }
    

}

