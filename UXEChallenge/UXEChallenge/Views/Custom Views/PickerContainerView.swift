//
//  PickerContainerView.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/15/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

enum TransitionVariable {
    case presentingDirection
    case zoomEffect
    case fade
    case barsAnimation
    case duration
    case motionCurve
    case damping
}

class PickerContainerView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var titleLabel: UILabel!
    
//    var transitionVariable: TransitionVariable!
    
//    
//    init(frame: CGRect) {
//        
////        self.transitionVariable = transitionVariable
//        super.init(frame: frame)
//        commonInit()
//        
//    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        commonInit()
        
    }
    
    private func commonInit() {
        
        Bundle.main.loadNibNamed("PickerContainerView", owner: self, options: nil)
        addSubview(self.contentView)
        let bounds = self.bounds
        self.contentView.frame = bounds
        
    }
    

}

