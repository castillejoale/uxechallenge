//
//  SinglePhotoFooter.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

protocol SinglePhotoFooterDelegate {
    
    func shareWasPressed()
    
}

class SinglePhotoFooter: UIView {
    
    @IBOutlet weak var commentButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var addButtonTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet var contentView: UIView!
    
    var delegate:SinglePhotoFooterDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("SinglePhotoFooter", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        
    }
    
    func updateInnerConstraints() {
        
        if let window = UIApplication.shared.keyWindow {
            let screenRect: CGRect = UIScreen.main.bounds
            let screenSafeWidth: CGFloat = screenRect.size.width - window.safeAreaInsets.left - window.safeAreaInsets.right
            let constant = ((screenSafeWidth - 57) / 3.0) - 57
            self.commentButtonLeadingConstraint.constant = constant
            self.addButtonTrailingConstraint.constant = constant
            self.frame.size.width = screenSafeWidth
        }
        
    }
    
    @IBAction func shareWasPressed(_ sender: Any) {
        self.delegate?.shareWasPressed()
    }
    
    
}

