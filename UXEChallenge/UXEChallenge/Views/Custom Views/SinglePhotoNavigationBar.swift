//
//  SinglePhotoNavigationBar.swift
//  UXEChallenge
//
//  Created by Alejandro Castillejo on 8/9/18.
//  Copyright © 2018 alejandrocastillejo. All rights reserved.
//

import UIKit

protocol SinglePhotoNavigationBarDelegate {
    
    func closeWasPressed()
    
}

class SinglePhotoNavigationBar: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var delegate:SinglePhotoNavigationBarDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("SinglePhotoNavigationBar", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    
    func setTitle(title:String) {
        self.titleLabel.text = title
    }
    
    func getTitle() -> String? {
        return self.titleLabel.text
    }
    
    func setDissmissButton(left: Bool) {

        self.backButton.isHidden = !left
        self.closeButton.isHidden = left
        
    }
    
    @IBAction func closeWasPressed(_ sender: Any) {
        delegate?.closeWasPressed()
    }
    
    
}

